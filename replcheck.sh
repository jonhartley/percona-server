#!/bin/bash

if [[ $1 == '-h' || $1 == '--help' ]];then
    echo "Usage: $0 <user> <pass> <available_when_donor=0|1> <log_file> <available_when_readonly=0|1> <defaults_extra_file>"
    exit
fi

if [ -f /tmp/drain.lock ]; then
echo "Node is draining"
exit 1
fi

if [ -f /tmp/restore.lock ]; then
echo "Node is restoring from backup"
exit 1
fi

if [ -z "$LAG" ]; then
    LAG=30
fi

MYSQL_USERNAME="${1-monitor}" 
MYSQL_PASSWORD="${2-${MONITOR_PASSWORD}}" 
AVAILABLE_WHEN_DONOR=${3:-1}
ERR_FILE="${4:-/var/lib/mysql/clustercheck.log}" 
DEFAULTS_EXTRA_FILE=${6:-/etc/my.cnf}

#Timeout exists for instances where mysqld may be hung
TIMEOUT=10

EXTRA_ARGS="--protocol=TCP --host=127.0.0.1"
if [[ -n "$MYSQL_USERNAME" ]]; then
    EXTRA_ARGS="$EXTRA_ARGS --user=${MYSQL_USERNAME}"
fi
if [[ -n "$MYSQL_PASSWORD" ]]; then
    EXTRA_ARGS="$EXTRA_ARGS --password=${MYSQL_PASSWORD}"
fi
if [[ -r $DEFAULTS_EXTRA_FILE ]];then 
    MYSQL_CMDLINE="mysql --defaults-extra-file=$DEFAULTS_EXTRA_FILE --connect-timeout=$TIMEOUT ${EXTRA_ARGS}"
else 
    MYSQL_CMDLINE="mysql --connect-timeout=$TIMEOUT ${EXTRA_ARGS}"
fi
#
# Perform the query to check the replication state
#
SLAVE_STATUS=($($MYSQL_CMDLINE -e "SHOW SLAVE STATUS\G;" 2>${ERR_FILE} | grep -E 'Slave_IO_Running:|Slave_SQL_Running:' | awk '{print $2}' | tr -d '\n'))
 
if [ ! "${SLAVE_STATUS}" = "YesYes" ]; then
    echo "Slave Not Running"
    exit 1
fi 

REPL_STATUS=($($MYSQL_CMDLINE -e "SHOW SLAVE STATUS\G;" 2>${ERR_FILE} | grep -E 'Seconds_Behind_Master:' | awk '{print $2}' | tr -d '\n'))

if [ ${REPL_STATUS} -gt $LAG ]; then
    echo "Slave is behind master by ${REPL_STATUS} seconds"
    exit 1
fi 