#!/bin/bash

mkdir -p /opt/mysql/etc/my.cnf.d
mkdir -p /opt/mysql/logs
mkdir -p /opt/mysql/data
mkdir -p /opt/mysql/bin
mkdir -p /opt/mysql/binlogs

for e in $(tr "\000" "\n" < /proc/1/environ); do
        eval "export $e"
        echo "$e"  >> /opt/mysql/logs/env.txt
done

getbytes() {
  for v in "${@:-$(</dev/stdin)}"
  do
    echo $v | awk \
      'BEGIN{IGNORECASE = 1}
       function printpower(n,b,p) {printf "%u\n", n*b^p; next}
       /[0-9]$/{print $1;next};
       /K(iB)?$/{printpower($1,  2, 10)};
       /M(iB)?$/{printpower($1,  2, 20)};
       /G(iB)?$/{printpower($1,  2, 30)};
       /T(iB)?$/{printpower($1,  2, 40)};
       /KB$/{    printpower($1, 10,  3)};
       /MB$/{    printpower($1, 10,  6)};
       /GB$/{    printpower($1, 10,  9)};
       /TB$/{    printpower($1, 10, 12)}'
  done
}

# ENV 
# CONFIG="max_allowed_packet=1G,innodb_buffer_pool_size=500M,max_connections=1000,wsrep_slave_threads=4"
# MYSQL_ROOT_PASSWORD=password                                                -- use kube secret
# BINLOG=true                                                                 -- create a binlog
# SLAVE=true                                                                  -- be a slave of another cluster
# REPLUSER=replication:password                                               -- replication credentials
# MASTER_HOST=pxc.srv.kubernetes                                              -- where your master is
# STARTFROMBACKUP=true                                                        -- restore backup before starting
# BACKUPFILE="backup-cluster-123456789.tar.gz"                                -- file to start from, if blank latest from BACKUPDIR choosen 
# BACKUPDIR=/mnt/backup                                                       -- location of latest backup
# SSL=client|all                                                              -- enable SSL - client: Just client traffic, all: client and wsrep, in CONFIG ssl-crt,ssl-key and ssl-ca required
# SCHEMA=<schema to create if not exist>                                      -- Create schema if not exist - must supply EXTUSER
# EXTUSER=<username>:<password>                                               -- Create user with full rights on SCHEMA - neither user or password can contain : using simple split
# SERVERID=1                                                                  -- create a static Sserver_id for replication (ip address is used if this is empty)
# RLIMIT="5M"                                                                 -- limit extraction of backup (as it can overwelm IO)

OIFS=$IFS;
IFS=",";

CONFIGVARS=(${CONFIG})
WSREPVARS=(${WSREPCONFIG})
AUDITVARS=(${AUDITCONFIG})

if [ "$SSL" = "client" ] || [ "$SSL" = "all" ]; then
# Check if options have been supplied for SSL
SSLOPTS=$(echo $CONFIG | awk '/ssl-cert=/ && /ssl-ca=/ && /ssl-key=/' | wc -l)
if [ $SSLOPTS -ne 1 ]; then
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 1 [Error] For SSL = 'client|all' must have ssl-cert,ssl-key and ssl-ca in extra config vars"
fi
fi

NODE_NAME=$(hostname)

echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Creating config from supplied env"

# Basic my.cnf, defaults used here in bytes, dont remove otherwise autotune wont work
cat > /opt/mysql/etc/my.cnf << EOF
[client]
default_character_set            = 'utf8mb4'

[mysql]
default_character_set            = 'utf8mb4'

[mysqld]
skip-character-set-client-handshake
collation_server                 = 'utf8mb4_unicode_ci'
character_set_server             = 'utf8mb4'
character_set_filesystem         = 'utf8mb4'
!includedir     /opt/mysql/etc/my.cnf.d/

disable-partition-engine-check
skip_name_resolve
explicit_defaults_for_timestamp  = 1
datadir                          = /opt/mysql/data
socket                           = /opt/mysql/data/mysql.sock
pid-file                         = /opt/mysql/data/mysqld.pid
long_query_time                  = 0.5
slow_query_log                   = ON
slow_query_log_file              = /opt/mysql/logs/slow-query.log
general_log_file                 = /opt/mysql/logs/general.log
log_error_verbosity              = 2
innodb_flush_method              = O_DIRECT
join_buffer_size                 = 262144
sort_buffer_size                 = 262144
key_buffer_size                  = 8
myisam_sort_buffer_size          = 4096
read_buffer_size                 = 131072
read_rnd_buffer_size             = 262144
innodb_log_buffer_size           = 16777216
innodb_buffer_pool_size          = 134217728
innodb_buffer_pool_chunk_size    = 134217728
innodb_page_size                 = 16384
innodb_buffer_pool_instances     = 1
max_connections                  = 101
wait_timeout                     = 300
interactive_timeout              = 300
table_definition_cache           = 6000
table_open_cache                 = 6000
innodb_flush_log_at_trx_commit   = 2
symbolic-links                   = OFF
net_buffer_length                = 16384
EOF

for ((i=0; i<${#CONFIGVARS[*]}; i++));
do
CONFIGNAME=$(echo ${CONFIGVARS[i]} | cut -d= -f1)
sed -i "/^${CONFIGNAME}/d" /opt/mysql/etc/my.cnf
OPT=$(echo ${CONFIGVARS[i]} | cut -d= -f1 | sed "s/[[:space:]]//g")
VAL=$(echo ${CONFIGVARS[i]} | cut -d= -f2 | sed "s/[[:space:]]//g")
if [[ "$VAL" =~ ^[0-9].* ]]; then
VAL=$( getbytes $VAL )
fi
echo "$(printf '%-30s %s ' ${OPT}) = $VAL" >> /opt/mysql/etc/my.cnf
done



if [ "$BINLOG" = "true" ]; then
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Creating binlog config from supplied env"
if [ -z "$SERVERID" ]; then
SERVERID=$(ip a | grep "inet.*eth0" | awk '{print $2}' | cut -d/ -f1 | awk -F. '{printf "%d%s", (($1*256+$2)*256+$3)*256+$4, RT}')
fi
cat > /opt/mysql/etc/my.cnf.d/binlog.cnf << EOH
[mysqld]
log-bin                          = /opt/mysql/binlogs/mysql-bin.log
log-bin-index                    = /opt/mysql/binlogs/mysql-bin.log.index
relay-log                        = /opt/mysql/binlogs/mysql-relay.bin
relay-log-index                  = /opt/mysql/binlogs/mysql-relay.bin.index
log-slave-updates                = ON
log-slow-slave-statements        = ON
expire-logs-days                 = 3
max-binlog-size                  = 200M
binlog-format                    = ROW
gtid-mode                        = ON
enforce-gtid-consistency         = ON
server_id                        = ${SERVERID}
master_info_repository           = TABLE 
relay_log_info_repository        = TABLE
sync_relay_log                   = 1
relay_log_recovery               = 1
sync_binlog                      = 1
EOH
fi

# Auto Tune
if [ -n "$AUTOTUNE" ] && [ -n "$MEM_LIMIT" ] && [ -n "CPU_LIMIT" ]; then


if [ "$AUTOTUNE" = "connections" ] || [ "$AUTOTUNE" = "bufferpool" ]; then
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Autotune set for $AUTOTUNE"
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Collecting variables already set in /opt/mysql/etc/my.cnf"
# grab variables (after they have been parsed fom CONFIG)
while IFS='' read -r line || [[ -n "$line" ]]; do
if [ ! "$line" = "" ]; then
line=$(echo "$line" | sed "s/[[:space:]]//g")
OPT=${line%=*}  # retain the part before the =
VAL=${line##*=}  # retain the part after the =
if [[ "$VAL" =~ ^[0-9]+$ ]]; then
eval "$OPT"=$VAL
fi
fi
done < /opt/mysql/etc/my.cnf
# Give ourselves a 10% buffer 
MEM_LIMIT=$(( (MEM_LIMIT/100) * 90 ))
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Giving a 10% buffer so memory limit is $MEM_LIMIT"
# connection mem, docs say each connection uses at leat 2Mb + extras
USERMEM=$((sort_buffer_size + myisam_sort_buffer_size + read_buffer_size + read_rnd_buffer_size + 2097152))
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Each connection will use $USERMEM bytes"
# Total connection memory
TOTUSERMEM=$(( USERMEM * max_connections ))
MEM=0

# Tune for max connections
if [ $AUTOTUNE = "connections" ]; then
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Tuning for max number of connections"
until [ $MEM -gt $((MEM_LIMIT-USERMEM)) ]; do
TOTUSERMEM=$(( USERMEM * max_connections ))
TOTSYSMEM=$(( key_buffer_size + innodb_buffer_pool_size + innodb_log_buffer_size + net_buffer_length ))
MEM=$(( TOTUSERMEM + TOTSYSMEM ))
max_connections=$((max_connections+1))
done
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Connections set at $max_connections"
fi

# Tune for max bufferpool
if [ $AUTOTUNE = "bufferpool" ]; then
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Tuning for bufferpool"
until [ $MEM -gt $((MEM_LIMIT-innodb_buffer_pool_chunk_size)) ]; do
TOTSYSMEM=$(( key_buffer_size + innodb_buffer_pool_size + innodb_log_buffer_size + net_buffer_length ))
MEM=$((TOTSYSMEM+TOTUSERMEM))
innodb_buffer_pool_size=$((innodb_buffer_pool_size+innodb_buffer_pool_chunk_size))
done
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Bufferpool size set at $innodb_buffer_pool_size bytes"
fi
# add the tuned values back into config before start
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Adding tuned values to /opt/mysql/etc/my.cnf"
sed -i "/^max_connections.*$/d" /opt/mysql/etc/my.cnf
sed -i "/^innodb_buffer_pool_size.*$/d" /opt/mysql/etc/my.cnf
echo "$(printf '%-36s %s ' innodb_buffer_pool_size) = $innodb_buffer_pool_size" >> /opt/mysql/etc/my.cnf
echo "$(printf '%-36s %s ' max_connections) = $max_connections" >> /opt/mysql/etc/my.cnf

# If memlimit not passed in or buffer pool less than 1G then use 1 instance
#POOLSIZE=$(cat /etc/my.cnf | grep innodb_log_buffer_size | cut -d= -f2 | sed "s/[[:space:]]//g")
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Calculating correct number of bufferpools for $innodb_buffer_pool_size"
if [ -z "$MEM_LIMIT" ] || [ $innodb_buffer_pool_size -lt 1073741824 ]; then
  sed -i "/^innodb_buffer_pool_instances.*$/d" /opt/mysql/etc/my.cnf
  BUFFPOOLS=1
fi

# If memlimit passed in and buffer pool > 1G - buffer pools = floor of buffer pool size / 1G
if [ -n "$MEM_LIMIT" ] && [ $innodb_buffer_pool_size -gt 1073741824 ]; then
BUFFPOOLS=$((innodb_buffer_pool_size / 1073741824))
 if [ -n "$CPU_LIMIT" ] && [ $BUFFPOOLS -gt $CPU_LIMIT ]; then
  BUFFPOOLS=$CPU_LIMIT
 fi
fi
sed -i "/^innodb_buffer_pool_instances.*$/d" /opt/mysql/etc/my.cnf
echo "$(printf '%-36s %s ' innodb_buffer_pool_instances) = ${BUFFPOOLS}" >> /opt/mysql/etc/my.cnf
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] bufferpools set at ${BUFFPOOLS}"
fi
fi

IFS=$OIFS


rm -f /opt/mysql/.my.cnf

# Find peers and status if exist
NODEID=$(echo $(hostname) | rev | cut -d- -f1 | rev)
HOSTPREFIX=$(echo $(hostname) | rev | cut -d- -f2- | rev)

INIT=-1
#0 = first node, no data dir & no start from backup
#1 = first node, no data dir & start from backup, backup dir exists
#2 = not first node, no data dir
#3 = not first node, data is present
#4 = data is present
if [ ! "${STARTFROMBACKUP}" = "true" ] && [ ! -d "/opt/mysql/data/mysql" ]; then 
  INIT=0
elif [ "${STARTFROMBACKUP}" = "true" ] && [ ! -d "/opt/mysql/data/mysql" ] && [ -n "${BACKUPDIR}" ]; then 
  INIT=1
elif [ -d "/opt/mysql/data/mysql" ]; then 
  INIT=2
fi
if [ $INIT -eq -1 ]; then
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 1 [Error] ENV variables not set correctly"
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 1 [Error] need at least:"
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 1 [Error] MYSQL_ROOT_PASSWORD=<password of all root users>"
exit 1
fi

case $INIT in
0)
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] This is the first start it has no grant tables - initialize"
rm -rf /opt/mysql/data/*
mysqld --defaults-file=/opt/mysql/etc/my.cnf --initialize-insecure --datadir=/opt/mysql/data/
mysqld --defaults-file=/opt/mysql/etc/my.cnf --datadir=/opt/mysql/data/ --skip-networking --log-error=/opt/mysql/logs/mysqld.log &
for i in {1..30}; do
  if $(mysql -u root -S /opt/mysql/data/mysql.sock -Nse "SELECT 1;" > /dev/null 2>&1) ; then
    break
  fi
  sleep 2
done
if [ $i -eq 30 ]; then
  echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 1 [Error] Failed to initialize"
  exit 1
fi
if $(/usr/bin/mysqladmin -S /opt/mysql/data/mysql.sock -u root password "${MYSQL_ROOT_PASSWORD}" > /dev/null 2>&1) ; then
cat > /opt/mysql/.my.cnf << EOF
[client]
user     = root
password = "${MYSQL_ROOT_PASSWORD}"
socket = "/opt/mysql/data/mysql.sock"
EOF
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Setting root password"
else
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 1 [Error] Failed to set root password"
exit 1
fi
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'root'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT ALL ON *.* TO 'root'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'monitor'@'127.0.0.1';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'monitor'@'127.0.0.1' IDENTIFIED BY '${MONITOR_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT SELECT,CREATE USER,REPLICATION CLIENT,SHOW DATABASES,SUPER,PROCESS ON *.* TO 'monitor'@'127.0.0.1';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'monitor'@'localhost';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'monitor'@'localhost' IDENTIFIED BY '${MONITOR_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT SELECT,CREATE USER,REPLICATION CLIENT,SHOW DATABASES,SUPER,PROCESS ON *.* TO 'monitor'@'localhost';"
if [ -n "${SCHEMA}" ] && [ -n "${EXTUSER}" ]; then # must be supplied <username>:<passowrd> (neither user or password can contain :)
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE SCHEMA IF NOT EXISTS ${SCHEMA};"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS '$(echo "${EXTUSER}" | cut -d: -f1)'@'%';"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER '$(echo "${EXTUSER}" | cut -d: -f1)'@'%' IDENTIFIED BY '$(echo "${EXTUSER}" | cut -d: -f2)';"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT ALL ON ${SCHEMA}.* TO '$(echo "${EXTUSER}" | cut -d: -f1)'@'%';"
fi
if [ "$AUDIT" = "true" ] || [ -n "${AUDITCONFIG}" ]; then
mysql -Nse "INSTALL PLUGIN server_audit SONAME 'server_audit.so';"
mysql -Nse "INSTALL PLUGIN connection_control SONAME 'connection_control.so';"
mysql -Nse "INSTALL PLUGIN connection_control_failed_login_attempts SONAME 'connection_control.so';"
fi
if [ -n "${REPLUSER}" ]; then
REPLUSR=$(echo ${REPLUSER} | cut -d: -f1)
REPLPAS=$(echo ${REPLUSER} | cut -d: -f2)
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS '${REPLUSR}'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER '${REPLUSR}'@'%' IDENTIFIED BY '${REPLPAS}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO '${REPLUSR}'@'%';"
fi
if [ $(ls /var/lib/sql/ | grep -c ".sql") -gt 0 ]; then
for f in $(ls /var/lib/sql/*.sql); do
  echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Adding SQL file ${f}"
  mysql -Nse "SOURCE ${f} ;"
done
fi
killall mysqld
until [ $(pgrep -c mysqld) -eq 0 ]; do sleep 1; done
;;
1)
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] No grant tables and is set to start from backup"
# backups done using #>innobackupex --stream=tar ./ | gzip - > backup.tar.gz
# create lock so Kubernetes does not restart pod - force pod to be live
touch /tmp/live.lock
if [ -z "${RLIMIT}" ]; then
  RLIMIT="5M"
fi
#chown -R mysql:mysql ${BACKUPDIR}
if [ -z "${BACKUPFILE}" ]; then
BACKUPFILE=$(ls ${BACKUPDIR}/ | grep ".tar.gz$" | sort | tail -n1)
fi
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Restoring ${BACKUPDIR}/${BACKUPFILE}"
pv -L ${RLIMIT} ${BACKUPDIR}/${BACKUPFILE} | zcat | tar -xiC /opt/mysql/data
innobackupex --apply-log /opt/mysql/data  > /dev/null 2>&1
mysqld --defaults-file=/opt/mysql/etc/my.cnf --datadir=/opt/mysql/data/ --skip-slave-start=ON --skip-networking --skip-grant-tables --log-error=/opt/mysql/logs/reset_root.log &
for i in {1..30}; do
  if $(mysql -S /opt/mysql/data/mysql.sock -Nse "SELECT 1;" > /dev/null 2>&1) ; then
    break
  fi
  sleep 2
done
if [ $i -eq 30 ]; then
  echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 1 [Error] Failed to reset root password on backup"
  exit 1
fi
mysql -u root -S /opt/mysql/data/mysql.sock -Nse "RESET MASTER;FLUSH PRIVILEGES;UPDATE mysql.user SET authentication_string=PASSWORD('${MYSQL_ROOT_PASSWORD}') WHERE User='root';FLUSH PRIVILEGES;"
killall mysqld
until [ $(pgrep -c mysqld) -eq 0 ]; do sleep 1; done
cat > /opt/mysql/.my.cnf << EOF
[client]
user     = root
password = "${MYSQL_ROOT_PASSWORD}"
socket = "/opt/mysql/data/mysql.sock"
EOF
mysqld --defaults-file=/opt/mysql/etc/my.cnf --datadir=/opt/mysql/data/ --skip-slave-start=ON --skip-networking --log-error=/opt/mysql/logs/mysqld.log &
for i in {1..30}; do
  if $(mysql -Nse "SELECT 1;" > /dev/null 2>&1) ; then
    break
  fi
  sleep 2
done
if [ $i -eq 30 ]; then
  echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 1 [Error] Failed to start to add users"
  exit 1
fi
mysql_upgrade
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'root'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT ALL ON *.* TO 'root'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'monitor'@'127.0.0.1';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'monitor'@'127.0.0.1' IDENTIFIED BY '${MONITOR_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT SELECT,CREATE USER,REPLICATION CLIENT,SHOW DATABASES,SUPER,PROCESS ON *.* TO 'monitor'@'127.0.0.1';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'monitor'@'localhost';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'monitor'@'localhost' IDENTIFIED BY '${MONITOR_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT SELECT,CREATE USER,REPLICATION CLIENT,SHOW DATABASES,SUPER,PROCESS ON *.* TO 'monitor'@'localhost';"
if [ -n "${SCHEMA}" ] && [ -n "${EXTUSER}" ]; then # must be supplied <username>:<passowrd> (neither user or password can contain :)
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE SCHEMA IF NOT EXISTS ${SCHEMA};"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS '$(echo "${EXTUSER}" | cut -d: -f1)'@'%';"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER '$(echo "${EXTUSER}" | cut -d: -f1)'@'%' IDENTIFIED BY '$(echo "${EXTUSER}" | cut -d: -f2)';"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT ALL ON ${SCHEMA}.* TO '$(echo "${EXTUSER}" | cut -d: -f1)'@'%';"
fi
if [ -n "${REPLUSER}" ]; then
REPLUSR=$(echo ${REPLUSER} | cut -d: -f1)
REPLPAS=$(echo ${REPLUSER} | cut -d: -f2)
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS '${REPLUSR}'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER '${REPLUSR}'@'%' IDENTIFIED BY '${REPLPAS}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO '${REPLUSR}'@'%';"
fi
if [ "$AUDIT" = "true" ] || [ -n "${AUDITCONFIG}" ]; then
mysql -Nse "INSTALL PLUGIN server_audit SONAME 'server_audit.so';"
mysql -Nse "INSTALL PLUGIN connection_control SONAME 'connection_control.so';"
mysql -Nse "INSTALL PLUGIN connection_control_failed_login_attempts SONAME 'connection_control.so';"
fi

if [ "${SLAVE}" = "true" ] && [ -n "${REPLUSER}" ] && [ -n "${MASTER_HOST}" ]; then # can only happen if started fom backup
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Getting GTID from backup"
REPLUSR=$(echo ${REPLUSER} | cut -d: -f1)
REPLPAS=$(echo ${REPLUSER} | cut -d: -f2)
if [ -f /opt/mysql/data/xtrabackup_binlog_info ]; then
LASTGTID=$(cat /opt/mysql/data/xtrabackup_binlog_info | tr -d "\n" | awk '{print $3}')
else
LASTGTID=$(cat /opt/mysql/data/xtrabackup_galera_info)
fi
if [ -z "$LASTGTID" ]; then
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Cant find last known position from restore - exiting"
sleep 360
exit 1
fi
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Attempting to connect to ${MASTER_HOST}"
mysql -Nse "STOP SLAVE;"
mysql -Nse "RESET SLAVE;"
mysql -Nse "RESET MASTER;"
mysql -Nse "SET GLOBAL GTID_PURGED=\"${LASTGTID}\";"
mysql -Nse "CHANGE MASTER TO MASTER_HOST=\"${MASTER_HOST}\", MASTER_USER=\"${REPLUSR}\", MASTER_PASSWORD=\"${REPLPAS}\", MASTER_PORT=3306, MASTER_AUTO_POSITION = 1;"
mysql -Nse "START SLAVE;"
#mysql -Nse "SET GLOBAL read_only=ON;"
if [ $(ls /var/lib/sql/ | grep -c ".sql") -gt 0 ]; then
for f in `ls /var/lib/sql/*.sql`; do
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Adding SQL file ${f}"
  mysql -Nse "SOURCE ${f} ;"
done
fi
echo "read_only=ON" >> /opt/mysql/etc/my.cnf
mysql -Nse "SET GLOBAL read_only=ON;"
sleep 5 # give it chance to connect
rm -f /tmp/live.lock
fi

killall mysqld
until [ $(pgrep -c mysqld) -eq 0 ]; do sleep 1; echo -n "."; done
echo ""
;;
2)
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Grant tables are present just attempt a start"
cat > /opt/mysql/.my.cnf << EOF
[client]
user     = root
password = "${MYSQL_ROOT_PASSWORD}"
socket = "/opt/mysql/data/mysql.sock"
EOF
;;
esac;

if [ -n "${BACKUPDIR}" ]; then
  cp -f /scripts/* /opt/mysql/bin/
  chmod +x /opt/mysql/bin/*
fi

if [ "$AUDIT" = "true" ] || [ -n "${AUDITCONFIG}" ]; then

if [ -n "${REPLUSER}" ]; then
EXREPLUSR=",$(echo ${REPLUSER} | cut -d: -f1)"
fi

cat > /opt/mysql/etc/my.cnf.d/audit.cnf << EOF
[mysqld]
plugin-load                                     = 'connection_control=connection_control.so'
plugin-load                                     = 'server_audit=server_audit.so'
server_audit_events                             = 'QUERY_DDL,QUERY_DML_NO_SELECT,QUERY_DCL'
server_audit_file_path                          = '/opt/mysql/logs/server_audit.log'
server_audit_file_rotate_size                   = 500000
server_audit_file_rotations                     = 2
server_audit_excl_users                         = 'monitor${EXREPLUSR}'
server_audit_logging                            = ON
server_audit_mode                               = 0
server_audit_output_type                        = file
server_audit_query_log_limit                    = 1024
connection_control_failed_connections_threshold = 3
connection_control_max_connection_delay         = 2147483647
connection_control_min_connection_delay         = 1000
EOF

for ((i=0; i<${#AUDITVARS[*]}; i++));
do
CONFIGNAME=$(echo ${AUDITVARS[i]} | cut -d= -f1)
sed -i "/^${CONFIGNAME}/d" /opt/mysql/etc/my.cnf.d/audit.cnf
OPT=$(echo ${AUDITVARS[i]} | cut -d= -f1 | sed "s/[[:space:]]//g")
VAL=$(echo ${AUDITVARS[i]} | cut -d= -f2 | sed "s/[[:space:]]//g")
if [[ "$VAL" =~ ^[0-9].* ]]; then
VAL=$( getbytes $VAL )
fi
echo "$(printf '%-30s %s ' ${OPT}) = $VAL" >> /opt/mysql/etc/my.cnf.d/audit.cnf
done

fi

pid=0
# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Trapped docker stop, shutting down MySQL"
    kill "$pid"
    wait "$pid"
  fi
  echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] MySQL stopped"
  exit 0;
}

# setup handlers
# on callback, kill the last background process, which is `tail -f /dev/null` and execute the specified handler
trap 'kill ${!}; term_handler' SIGKILL SIGTERM SIGHUP SIGINT EXIT

# run application
echo "$(date +"%Y-%m-%dT%H:%M:%S.%6NZ") 0 [Info] Starting MySQL"
eval "mysqld --defaults-file=/opt/mysql/etc/my.cnf --datadir=/opt/mysql/data/ &"
pid="$!"

# wait indefinitely
while true; do 
tail -f /dev/null
done
