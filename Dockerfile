FROM centos:7

ARG VERSION=5.7.33

RUN groupadd -g 65535 mysql; \
useradd -p --disabled-password -u 65535 -g mysql -s /bin/bash -d /opt/mysql mysql; \ 
yum -y upgrade; \
yum -y install epel-release; \
yum -y install https://repo.percona.com/yum/percona-release-latest.noarch.rpm; \
yum -y --nogpgcheck install \
Percona-Server-shared-compat-57-$VERSION \
Percona-Server-shared-57-$VERSION \
Percona-Server-server-57-$VERSION \
Percona-Server-client-57-$VERSION \
percona-toolkit \
percona-xtrabackup-24 \
pigz \
pv \
openssl \
psmisc \
nc \
iperf \
iproute \
sysbench; \
mkdir -p /var/lib/sql; \
mkdir -p /opt/mysql; \
chown -R mysql:mysql /opt/mysql; \
rm -f /etc/my.cnf; \
yum clean all; \
rm -rf /var/cache/yum;\
rm -rf /var/lib/mysql/*; \
rm -rf /var/lib/yum/*; \
rm -rf /tmp/*

COPY server_audit.so /usr/lib64/mysql/plugin/server_audit.so
COPY entrypoint.sh /entrypoint.sh
COPY livecheck.sh /usr/bin/livecheck.sh
COPY replcheck.sh /usr/bin/replcheck.sh
COPY bin/ scripts/

USER mysql

EXPOSE 3306
ENTRYPOINT ["/entrypoint.sh"]




